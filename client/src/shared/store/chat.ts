import { createSlice } from '@reduxjs/toolkit';

type Message = {
  createdtime: number;
  message: string;
  user: string;
};

type User = {
  id: string;
  room: string;
  user: string;
};

type ChatReducer = {
  user: string;
  room: string;
  messages: Array<Message>;
  roomUsers: Array<User>;
};

const initialState: ChatReducer = {
  user: '',
  room: '',
  messages: [],
  roomUsers: [],
};

const chat = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    joinRoom: (state, action) => {},
    leaveRoom: (state, action) => {},

    startListeningUsers: () => {},
    startListeningMessages: () => {},
    startListeningMessage: () => {},

    getMessages: (state, action) => {
      state.messages = action.payload;
    },
    getRoomUsers: (state, action) => {
      state.roomUsers = action.payload;
    },
    getMessage: (state, action) => {
      state.messages = [...state.messages, action.payload];
    },
    sendMessage: (state, action) => {},

    setUser: (state, action) => {
      state.user = action.payload;
    },
    setRoom: (state, action) => {
      state.room = action.payload;
    },
  },
});

export const {
  startListeningUsers,
  startListeningMessages,
  startListeningMessage,
  getMessages,
  getRoomUsers,
  getMessage,
  setUser,
  setRoom,
  joinRoom,
  leaveRoom,
  sendMessage,
} = chat.actions;

export default chat.reducer;
