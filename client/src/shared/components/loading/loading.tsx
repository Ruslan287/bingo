import CircularProgress, { CircularProgressProps } from '@mui/material/CircularProgress';
import cx from 'classnames';

import { AppWithStyles } from '@core/theme/types/main';

import { useStyles } from './loading.styles';

export type LoadingProps = AppWithStyles<typeof useStyles> &
  Omit<CircularProgressProps, 'classes'> & {
    margin?: 'none' | 'small' | 'normal' | 'big';
    absolute?: boolean;
    height?: number | string;
  };

export const Loading: React.FC<LoadingProps> = ({ classes: externalClasses, margin = 'none', size = 40, absolute, height, className, ...otherProps }) => {
  const { classes } = useStyles(undefined, { props: { classes: externalClasses } });

  return (
    <div style={{ height }} className={cx(classes.root, className, classes[margin], { [classes.rootAbsolute]: absolute })}>
      <CircularProgress size={size} classes={{ svg: classes.svg }} {...otherProps} />
    </div>
  );
};
