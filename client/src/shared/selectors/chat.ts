import { createSelector } from 'reselect';
import { RootState } from '@shared/store';

const R = (store: RootState) => store;

export const chat = createSelector(R, (r) => r.chat);
