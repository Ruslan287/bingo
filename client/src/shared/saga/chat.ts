import io from 'socket.io-client';
import { takeLatest, call, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import {
  startListeningUsers,
  startListeningMessages,
  startListeningMessage,
  getMessages,
  getRoomUsers,
  getMessage,
  joinRoom,
  leaveRoom,
  sendMessage,
} from '../store/chat';

interface RequestProps {
  [key: string]: any;
}

type ChannelType = ReturnType<typeof createChannel>;

const socket = io('http://localhost:4000');

function createChannel(socketEvent: string) {
  const subscribe = (emitter: any) => {
    socket.on(socketEvent, emitter);

    return () => socket.removeListener(socketEvent, emitter);
  };

  return eventChannel(subscribe);
}

export function* chatroom_users() {
  const channel: ChannelType = yield call(createChannel, 'chatroom_users');

  while (true) {
    const data: ChannelType = yield take(channel);
    yield put(getRoomUsers(data));
  }
}
export function* last_100_messages() {
  const channel: ChannelType = yield call(createChannel, 'last_100_messages');
  while (true) {
    const data: ChannelType = yield take(channel);
    yield put(getMessages(data));
  }
}
export function* receive_message() {
  const channel: ChannelType = yield call(createChannel, 'receive_message');
  while (true) {
    const data: ChannelType = yield take(channel);
    yield put(getMessage(data));
  }
}

function* startListeningUsersFunc() {
  yield call(chatroom_users);
}
function* startListeningMessagesFunc() {
  yield call(last_100_messages);
}
function* startListeningMessageFunc() {
  yield call(receive_message);
}

//////
function* joinRoomFunc(request: RequestProps): any {
  const { user, room } = request.payload;
  socket.emit('join_room', { user, room });
}

function* leaveRoomFunc(request: RequestProps): any {
  const { user, room } = request.payload;
  socket.emit('leave_room', { user, room });
}

function* sendMessageFunc(request: RequestProps): any {
  const { user, room, message, createdtime } = request.payload;
  socket.emit('send_message', { user, room, message, createdtime });
}

export default function* chat() {
  yield takeLatest([startListeningUsers], startListeningUsersFunc);
  yield takeLatest([startListeningMessages], startListeningMessagesFunc);
  yield takeLatest([startListeningMessage], startListeningMessageFunc);
  yield takeLatest([joinRoom], joinRoomFunc);
  yield takeLatest([leaveRoom], leaveRoomFunc);
  yield takeLatest([sendMessage], sendMessageFunc);
}
