import './polyfill';
import 'reset-css';
import 'react-hot-loader';

import { StrictMode } from 'react';
import { BrowserRouter } from 'react-router-dom';

import ReactDOM from 'react-dom/client';
import { AppThemeProvider } from '@core/theme/provider';
import { AppCssBaseline } from '@core/theme/utils/css-baseline';
import { appCreateTheme } from '@core/theme';

import { LazyLoad } from '@shared/components/lazy-load';

import store from '@shared/store';
import { Provider } from 'react-redux';

const App = React.lazy(() => import('./app'));

const theme = appCreateTheme();

async function initializeApp() {
  const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

  root.render(
    <StrictMode>
      <BrowserRouter>
        <AppThemeProvider theme={theme}>
          <AppCssBaseline />
          <LazyLoad>
            <Provider store={store}>
              <App />
            </Provider>
          </LazyLoad>
        </AppThemeProvider>
      </BrowserRouter>
    </StrictMode>
  );
}

initializeApp();
