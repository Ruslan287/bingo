type BaseCSSRules = import('csstype').Properties<string>;

declare interface CSSRules extends BaseCSSRules {
  [key: string]: unknown | BaseCSSRules;
}
