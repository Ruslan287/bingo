import { appMakeStyles } from '@core/theme/utils/make-styles';

export const useStyles = appMakeStyles()(() => ({
  chatContainer: {
    maxWidth: '1100px',
    height: '100%',
    margin: '0 auto',
    display: 'grid',
    gridTemplateColumns: '1fr 4fr',
    gap: 20,
  },
}));
