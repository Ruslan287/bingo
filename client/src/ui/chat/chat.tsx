import { AppWithStyles } from '@core/theme/types/main';
import { useTypeDispatch } from '@shared/hooks/useTypeRedux';

import { useStyles } from './chat.styles';

import { MessagesReceived } from '../messages-received';
import { RoomUsers } from '../room-users';
import { SendMessageInput } from '../send-message';
import { startListeningUsers, startListeningMessages, startListeningMessage } from '@shared/store/chat';

export type ChatProps = AppWithStyles<typeof useStyles> & {};

export const Chat: React.FC<ChatProps> = () => {
  const { classes } = useStyles();

  const dispatch = useTypeDispatch();

  React.useEffect(() => {
    dispatch(startListeningUsers());
    dispatch(startListeningMessages());
    dispatch(startListeningMessage());
  }, [dispatch]);

  return (
    <div className={classes.chatContainer}>
      <RoomUsers />

      <div>
        <MessagesReceived />
        <SendMessageInput />
      </div>
    </div>
  );
};
