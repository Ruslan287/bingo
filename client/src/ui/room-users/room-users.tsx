import { useNavigate } from 'react-router-dom';
import { AppWithStyles } from '@core/theme/types/main';

import { useTypedSelector, useTypeDispatch } from '@shared/hooks/useTypeRedux';
import { chat } from '@shared/selectors/chat';
import { leaveRoom } from '@shared/store/chat';

import { useStyles } from './room-users.styles';

export type RoomUsersProps = AppWithStyles<typeof useStyles> & {};

export const RoomUsers: React.FC<RoomUsersProps> = () => {
  const { classes } = useStyles();

  const navigate = useNavigate();
  const dispatch = useTypeDispatch();
  const { roomUsers, user, room } = useTypedSelector(chat);

  const handleLeaveRoom = () => {
    dispatch(leaveRoom({ user, room }));

    navigate('/', { replace: true });
  };

  return (
    <div className={classes.roomAndUsersColumn}>
      <h2 className={classes.roomTitle}>{room}</h2>

      <div>
        {roomUsers.length > 0 && <h5 className={classes.usersTitle}>Users:</h5>}
        <ul className={classes.usersList}>
          {roomUsers.map((item, i) => (
            <li
              style={{
                fontWeight: `${item.user === user ? 'bold' : 'normal'}`,
              }}
              key={i}
            >
              {item.user}
            </li>
          ))}
        </ul>
      </div>

      <button className={classes.btn} onClick={handleLeaveRoom}>
        Leave
      </button>
    </div>
  );
};
