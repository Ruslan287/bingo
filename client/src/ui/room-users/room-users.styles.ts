import { appMakeStyles } from '@core/theme/utils/make-styles';

export const useStyles = appMakeStyles()(() => ({
  roomAndUsersColumn: {
    borderRight: '1px solid #dfdfdf',
    background: '#ccc',
  },
  roomTitle: {
    marginBottom: '60px',
    textTransform: 'uppercase',
    fontSize: '1.2rem',
    textAlign: 'center',
    marginTop: '50px',
  },
  usersTitle: {
    fontSize: '1.2rem',
    textAlign: 'center',
  },
  usersList: {
    listStyleType: 'none',
    paddingLeft: 0,
    marginBottom: '60px',
    color: 'rgb(153, 217, 234)',
    '&>li': {
      color: '#000',
      textAlign: 'center',
    },
  },
  btn: {
    margin: '0 auto',
    display: 'block',
  },
}));
