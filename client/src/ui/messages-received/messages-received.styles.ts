import { appMakeStyles } from '@core/theme/utils/make-styles';

export const useStyles = appMakeStyles()(() => ({
  messagesColumn: {
    height: '85vh',
    overflow: 'auto',
    padding: '10px 10px 10px 40px',
  },
  message: {
    background: 'rgb(0, 24, 111)',
    borderRadius: '6px',
    marginBottom: '24px',
    maxWidth: '600px',
    padding: '12px',
  },
  msgMeta: {
    color: 'rgb(153, 217, 234)',
    fontSize: '0.75rem',
  },
  msgText: {
    color: '#fff',
  },
}));
