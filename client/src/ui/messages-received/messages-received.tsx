import { AppWithStyles } from '@core/theme/types/main';

import { useTypedSelector } from '@shared/hooks/useTypeRedux';
import { chat } from '@shared/selectors/chat';

import { useStyles } from './messages-received.styles';

export type MessagesReceivedProps = AppWithStyles<typeof useStyles> & {};

export const MessagesReceived: React.FC<MessagesReceivedProps> = () => {
  const { classes } = useStyles();

  const { messages } = useTypedSelector(chat);

  const messagesColumnRef = React.useRef(null);

  function formatDateFromTimestamp(timestamp: number) {
    const date = new Date(timestamp);
    return date.toLocaleString();
  }

  return (
    <div className={classes.messagesColumn} ref={messagesColumnRef}>
      {messages.map((msg, i) => (
        <div className={classes.message} key={i}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span className={classes.msgMeta}>{msg.user}</span>
            <span className={classes.msgMeta}>{formatDateFromTimestamp(msg.createdtime)}</span>
          </div>
          <p className={classes.msgText}>{msg.message}</p>
          <br />
        </div>
      ))}
    </div>
  );
};
