import { appMakeStyles } from '@core/theme/utils/make-styles';

export const useStyles = appMakeStyles()(() => ({
  sendMessageContainer: {
    padding: '16px 20px 20px 16px',
  },
  messageInput: {
    padding: '14px',
    marginRight: '16px',
    width: '60%',
    borderRadius: '6px',
    border: '1px solid rgb(153, 217, 234)',
    fontSize: '0.9rem',
  },
}));
