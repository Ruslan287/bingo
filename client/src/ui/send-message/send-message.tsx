import { AppWithStyles } from '@core/theme/types/main';

import { useTypedSelector, useTypeDispatch } from '@shared/hooks/useTypeRedux';
import { chat } from '@shared/selectors/chat';
import { sendMessage } from '@shared/store/chat';

import { useStyles } from './send-message.styles';

export type SendMessageInputProps = AppWithStyles<typeof useStyles> & {};

export const SendMessageInput: React.FC<SendMessageInputProps> = () => {
  const { classes } = useStyles();

  const dispatch = useTypeDispatch();
  const { user, room } = useTypedSelector(chat);
  const [message, setMessage] = React.useState('');

  const handleSendMessage = () => {
    if (message !== '') {
      const createdtime = Date.now();
      dispatch(sendMessage({ user, room, message, createdtime }));
      setMessage('');
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      handleSendMessage();
    }
  };

  return (
    <div className={classes.sendMessageContainer}>
      <input
        className={classes.messageInput}
        placeholder="Message..."
        onChange={(e) => setMessage(e.target.value)}
        onKeyDown={handleKeyDown}
        value={message}
      />
      <button className="btn btn-primary" onClick={handleSendMessage}>
        Send Message
      </button>
    </div>
  );
};
