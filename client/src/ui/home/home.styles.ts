import { appMakeStyles } from '@core/theme/utils/make-styles';

export const useStyles = appMakeStyles()(() => ({
  container: {
    height: '100vh',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'rgb(63, 73, 204)',
  },
  formContainer: {
    width: '400px',
    margin: '0 auto 0 auto',
    padding: '32px',
    background: 'lightblue',
    borderRadius: '6px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: '28px',
  },
  input: {
    width: '100%',
    padding: '12px',
    borderRadius: '6px',
    border: '1px solid rgb(63, 73, 204)',
    fontSize: '0.9rem',
  },
}));
