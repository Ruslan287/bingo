import { useNavigate } from 'react-router-dom';
import { AppWithStyles } from '@core/theme/types/main';

import { useTypedSelector, useTypeDispatch } from '@shared/hooks/useTypeRedux';
import { chat } from '@shared/selectors/chat';
import { setUser, setRoom, joinRoom } from '@shared/store/chat';

import { useStyles } from './home.styles';

export type HomeProps = AppWithStyles<typeof useStyles> & {};

export const Home: React.FC<HomeProps> = () => {
  const { classes } = useStyles();

  const navigate = useNavigate();
  const dispatch = useTypeDispatch();
  const { room, user } = useTypedSelector(chat);

  const handleJoinRoom = () => {
    if (room !== '' && user !== '') {
      dispatch(joinRoom({ user, room }));
      navigate('/chat', { replace: true });
    }
  };

  const handleSetUser = (user: string) => {
    dispatch(setUser(user));
  };

  const handleSetRoom = (room: string) => {
    dispatch(setRoom(room));
  };

  return (
    <div className={classes.container}>
      <div className={classes.formContainer}>
        <h1>{`Rooms`}</h1>
        <input className={classes.input} placeholder="user..." onChange={(e) => handleSetUser(e.target.value)} />

        <select className={classes.input} onChange={(e) => handleSetRoom(e.target.value)}>
          <option>Select Room</option>
          <option value="Room One">Room One</option>
          <option value="Room One">Room Two</option>
        </select>

        <button className="btn btn-secondary" style={{ width: '100%' }} onClick={handleJoinRoom}>
          Join Room
        </button>
      </div>
    </div>
  );
};
