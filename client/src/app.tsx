import { Routes, Route } from 'react-router-dom';

import { Home } from '@ui/home';
import { Chat } from '@ui/chat';

const App: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/chat" element={<Chat />} />
    </Routes>
  );
};

export default App;
