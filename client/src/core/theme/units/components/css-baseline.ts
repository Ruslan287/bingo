import { AppComponents } from '@core/theme/types/main';

export const initializeCssBaselineOverrides = (config?: AppComponents['MuiCssBaseline']): AppComponents['MuiCssBaseline'] => {
  return {
    ...config,
    styleOverrides: {
      '@global': {
        '#root': {
          height: '100%',
        },
        a: {
          cursor: 'pointer',
        },
        input: {
          '&::-ms-clear, &::-ms-reveal': {
            display: 'none',
          },
        },
      },
    },
  };
};
