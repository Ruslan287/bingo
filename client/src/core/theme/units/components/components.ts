import { AppThemeOptions } from '@core/theme/types/main';

export const initializeComponents = (config?: AppThemeOptions['components']): AppThemeOptions['components'] => {
  return {
    ...config,
    MuiTextField: {
      styleOverrides: {
        root: {
          margin: '0px !important',
        },
      },
      defaultProps: {
        hiddenLabel: true,
        size: 'small',
      },
    },
    MuiTooltip: {
      defaultProps: {
        arrow: true,
      },
      styleOverrides: {
        tooltip: {
          backgroundColor: '#18234C',
          color: '#FFFFFF',
          fontSize: 12,
          fontWeight: 400,
        },
        arrow: {
          color: '#18234C',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          boxShadow: 'none',
          textTransform: 'none',
          borderRadius: 32,
          height: 34,
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          padding: 0,
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          borderRadius: 16,
          boxShadow: '0px 4px 12px rgb(20 20 52 / 8%)',
          overflow: 'hidden',
          border: '1px solid #DFDFE2',
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          width: '100%',
        },
      },
    },
  };
};
