import { AppShadows } from '@core/theme/types/main';

export const initializeShadows = (defaultShadows: AppShadows, customShadows?: AppShadows): AppShadows => {
  const mergedShadows = defaultShadows.map((shadow, i) => {
    if (customShadows?.length && customShadows[i]) {
      return customShadows[i];
    }

    return shadow;
  });

  return mergedShadows as unknown as AppShadows;
};
