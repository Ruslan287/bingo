import { AppPaletteOptions } from '@core/theme/types/main';

export const initializePalette = (config?: AppPaletteOptions): AppPaletteOptions => {
  return {
    text: {
      primary: '#18234C',
      secondary: '#565861',
      disabled: '#93959F',
    },
    grey: {
      50: '#B2B2B2',
      100: '#DFDFE2',
      200: '#808080',
    },
    primary: {
      main: '#18234C',
      light: '#E9ECFC',
    },
    background: {
      paper: '#fff',
      default: '#F7F8FC',
    },
    info: {
      main: '#E9ECFC',
    },
    success: {
      main: '#EBF4EF',
    },
    error: {
      main: '#D4351C',
    },
    ...config,
  };
};
