import { ThemeOptions } from '@mui/material/styles/createTheme';
export { PaletteOptions as AppPaletteOptions } from '@mui/material/styles/createPalette';
export { ComponentsProps as AppComponentsProps } from '@mui/material/styles/props';
export { TypographyOptions as AppTypographyOptions } from '@mui/material/styles/createTypography';
export { ZIndexOptions as AppZIndexOptions } from '@mui/material/styles/zIndex';
export { Shadows as AppShadows } from '@mui/material/styles/shadows';
export { MixinsOptions as AppMixinsOptions } from '@mui/material/styles/createMixins';
export {
  Theme as AppTheme,
  Direction as AppDirection,
  ThemeOptions as AppThemeOptions,
  TransitionsOptions as AppTransitionsOptions,
  Components as AppComponents,
} from '@mui/material/styles';

export type AppSpacingOptions = ThemeOptions['spacing'];
export type AppShapeOptions = ThemeOptions['shape'];
export type AppBreakpointsOptions = ThemeOptions['breakpoints'];
export type AppWithStyles<T extends () => { classes: unknown }> = {
  classes?: Partial<ReturnType<T>['classes']>;
};
