import { AppThemeOptions } from './types/main';
import { initializeBreakpoints } from './units/breakpoints';
import { initializeDirection } from './units/direction';
import { initializeMixins } from './units/mixins';
import { initializeComponents } from './units/components';
import { initializePalette } from './units/palette';
import { initializeShadows } from './units/shadows';
import { initializeShape } from './units/shape';
import { initializeSpacing } from './units/spacing';
import { initializeTransitions } from './units/transitions';
import { initializeTypography } from './units/typography';
import { appCreateTheme as appCreateThemeCore } from './utils/theme';

export function appCreateTheme(options?: AppThemeOptions) {
  const baseTheme = appCreateThemeCore();

  const theme = appCreateThemeCore({
    breakpoints: initializeBreakpoints(options?.breakpoints),
    direction: initializeDirection(options?.direction),
    mixins: initializeMixins(options?.mixins),
    palette: initializePalette(options?.palette),
    components: initializeComponents(options?.components),
    shadows: initializeShadows(baseTheme.shadows, options?.shadows),
    shape: initializeShape(options?.shape),
    spacing: initializeSpacing(options?.spacing),
    transitions: initializeTransitions(options?.transitions),
    typography: initializeTypography(options?.typography),
  });

  return theme;
}
